package com.rahmanarifofficial.vidiochucknorris.viewmodel

import androidx.lifecycle.ViewModel
import com.rahmanarifofficial.vidiochucknorris.repository.JokesRepository

class JokesViewModel(private val repository: JokesRepository) : ViewModel() {

    fun getJokes(query: String) = repository.getJokes(query)

    fun getCategoryJokes() = repository.getCategoryJokes()

    fun getJokesByCategory(category: String) = repository.getJokesByCategory(category)
}