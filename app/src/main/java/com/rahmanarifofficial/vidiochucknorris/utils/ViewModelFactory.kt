package com.rahmanarifofficial.vidiochucknorris.utils

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rahmanarifofficial.vidiochucknorris.repository.JokesRepository
import com.rahmanarifofficial.vidiochucknorris.viewmodel.JokesViewModel

class ViewModelFactory private constructor(
    private val dataRepository: JokesRepository
) : ViewModelProvider.NewInstanceFactory() {
    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(): ViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ViewModelFactory(
                    Injection.provideJokesRepository()
                )
            }
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(JokesViewModel::class.java) -> {
                JokesViewModel(dataRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
    }

}