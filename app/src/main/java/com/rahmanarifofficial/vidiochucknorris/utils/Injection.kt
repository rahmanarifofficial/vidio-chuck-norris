package com.rahmanarifofficial.vidiochucknorris.utils

import com.rahmanarifofficial.vidiochucknorris.datasource.DataSource
import com.rahmanarifofficial.vidiochucknorris.repository.JokesRepository

object Injection {
    fun provideJokesRepository(): JokesRepository {
        return JokesRepository(DataSource.Service.api, AppExecutors())
    }
}