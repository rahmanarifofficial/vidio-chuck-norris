package com.rahmanarifofficial.vidiochucknorris.utils

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        provideLayout()
        initObject(savedInstanceState)
        initUI(savedInstanceState)
        initData(savedInstanceState)
        initListener(savedInstanceState)
    }

    protected abstract fun provideLayout()

    protected abstract fun initObject(bundle: Bundle?)

    protected abstract fun initUI(bundle: Bundle?)

    protected abstract fun initData(bundle: Bundle?)

    protected abstract fun initListener(bundle: Bundle?)


}