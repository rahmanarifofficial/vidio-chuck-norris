package com.rahmanarifofficial.vidiochucknorris.repository

import androidx.lifecycle.LiveData
import com.rahmanarifofficial.vidiochucknorris.datasource.ApiResponse
import com.rahmanarifofficial.vidiochucknorris.datasource.ApiService
import com.rahmanarifofficial.vidiochucknorris.datasource.NetworkOnlyResource
import com.rahmanarifofficial.vidiochucknorris.datasource.Resource
import com.rahmanarifofficial.vidiochucknorris.model.Jokes
import com.rahmanarifofficial.vidiochucknorris.model.ResJokes
import com.rahmanarifofficial.vidiochucknorris.utils.AppExecutors
import javax.inject.Singleton

@Singleton
class JokesRepository(
    private val jokesService: ApiService,
    private val appExecutors: AppExecutors
) {

    fun getJokes(query: String): LiveData<Resource<ResJokes>> {
        return object : NetworkOnlyResource<ResJokes, ResJokes>(appExecutors) {
            override fun handleCallResult(item: ResJokes?): ResJokes? {
                return item
            }

            override fun createCall(): LiveData<ApiResponse<ResJokes>> {
                return jokesService.searchJokes(query)
            }

        }.asLiveData()
    }

    fun getCategoryJokes(): LiveData<Resource<List<String>>> {
        return object : NetworkOnlyResource<List<String>, List<String>>(appExecutors) {
            override fun handleCallResult(item: List<String>?): List<String>? {
                return item
            }

            override fun createCall(): LiveData<ApiResponse<List<String>>> {
                return jokesService.categoryJokes()
            }

        }.asLiveData()
    }

    fun getJokesByCategory(category: String): LiveData<Resource<Jokes>> {
        return object : NetworkOnlyResource<Jokes, Jokes>(appExecutors) {
            override fun handleCallResult(item: Jokes?): Jokes? {
                return item
            }

            override fun createCall(): LiveData<ApiResponse<Jokes>> {
                return jokesService.getJokesByCategory(category)
            }

        }.asLiveData()
    }
}