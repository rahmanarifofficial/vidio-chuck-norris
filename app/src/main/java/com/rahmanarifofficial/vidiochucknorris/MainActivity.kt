package com.rahmanarifofficial.vidiochucknorris

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.rahmanarifofficial.vidiochucknorris.adapter.RecyclerViewAdapter
import com.rahmanarifofficial.vidiochucknorris.datasource.Status
import com.rahmanarifofficial.vidiochucknorris.model.Jokes
import com.rahmanarifofficial.vidiochucknorris.utils.BaseActivity
import com.rahmanarifofficial.vidiochucknorris.utils.Utils.goToActivity
import com.rahmanarifofficial.vidiochucknorris.utils.ViewModelFactory
import com.rahmanarifofficial.vidiochucknorris.viewmodel.JokesViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.error_layout.*


class MainActivity : BaseActivity() {
    private lateinit var jokesViewModel: JokesViewModel
    private lateinit var adapter: RecyclerViewAdapter

    private var itemListCategory = ArrayList<String>()
    private var itemListJokes = ArrayList<Jokes>()

    private lateinit var searchView: SearchView

    override fun provideLayout() {
        setContentView(R.layout.activity_main)
    }

    override fun initObject(bundle: Bundle?) {
        val factory = ViewModelFactory.getInstance()
        jokesViewModel = ViewModelProvider(this, factory)[JokesViewModel::class.java]

        adapter = RecyclerViewAdapter(
            this,
            itemListCategory,
            itemListJokes,
            onCategoryClick = { position: Int, item: String ->
                val b = Bundle()
                b.putString("CATEGORY_BUNDLE", item)
                goToActivity(DetailJokeActivity::class.java, b)
            })
    }

    override fun initUI(bundle: Bundle?) {
        listRecycle?.layoutManager = LinearLayoutManager(this)
        listRecycle?.adapter = adapter
    }

    override fun initData(bundle: Bundle?) {
        loadDataCategory()
    }

    override fun initListener(bundle: Bundle?) {
        buttonRefreshError?.setOnClickListener {
            searchView.setQuery("", false)
            searchView.clearFocus()
            loadDataCategory()
        }
    }

    private fun loadDataCategory() {
        jokesViewModel.getCategoryJokes().observe(this, {
            it?.let { res ->
                when (res.status) {
                    Status.SUCCESS -> {
                        progressBar?.visibility = View.GONE
                        layoutError?.visibility = View.GONE
                        tvTitleCategory?.visibility = View.VISIBLE
                        val data = res.data
                        itemListJokes.clear()
                        itemListCategory.clear()
                        if (!data.isNullOrEmpty()) {
                            itemListCategory.addAll(data)
                        } else {
                            layoutError?.visibility = View.VISIBLE
                            textTitleError?.text = getString(R.string.title_empty_data)
                            textSubtitleError?.text = ""
                        }
                        adapter.notifyDataSetChanged()
                    }
                    Status.ERROR -> {
                        progressBar?.visibility = View.GONE
                        layoutError?.visibility = View.VISIBLE
                    }
                    Status.LOADING -> {
                        progressBar?.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun loadDataJokes(query: String) {
        jokesViewModel.getJokes(query).observe(this, {
            it?.let { res ->
                when (res.status) {
                    Status.SUCCESS -> {
                        progressBar?.visibility = View.GONE
                        tvTitleCategory?.visibility = View.GONE
                        layoutError?.visibility = View.GONE
                        val data = res.data?.result
                        itemListCategory.clear()
                        itemListJokes.clear()
                        if (!data.isNullOrEmpty()) {
                            itemListJokes.addAll(data)
                        } else {
                            layoutError?.visibility = View.VISIBLE
                            textTitleError?.text = getString(R.string.title_empty_data)
                            textSubtitleError?.text = "Tidak ada data untuk keyword ${query}"
                            buttonRefreshError?.visibility = View.GONE
                        }
                        adapter.notifyDataSetChanged()
                    }
                    Status.ERROR -> {
                        progressBar?.visibility = View.GONE
                        layoutError?.visibility = View.VISIBLE
                        textTitleError?.text = getString(R.string.masalah_internet)
                        textSubtitleError?.text = getString(R.string.oops_terjadi_kesalahan)
                        buttonRefreshError?.visibility = View.VISIBLE
                    }
                    Status.LOADING -> {
                        progressBar?.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        searchView = menu!!.findItem(R.id.action_search).actionView as SearchView
        searchView.queryHint = getString(R.string.search)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    if (newText.length >= 3) {
                        loadDataJokes(newText)
                    } else {
                        loadDataCategory()
                    }
                } ?: apply {
                    loadDataCategory()
                }
                return false
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            1 -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}