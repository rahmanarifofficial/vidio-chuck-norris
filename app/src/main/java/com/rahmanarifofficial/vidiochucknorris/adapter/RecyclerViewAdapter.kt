package com.rahmanarifofficial.vidiochucknorris.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rahmanarifofficial.vidiochucknorris.R
import com.rahmanarifofficial.vidiochucknorris.model.Jokes
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_list_category.view.*
import kotlinx.android.synthetic.main.item_list_jokes.view.*

class RecyclerViewAdapter(
    private val context: Context,
    private val itemListCategory: List<String>,
    private val itemListJokes: List<Jokes>,
    private val onCategoryClick: ((Int, String) -> Unit)? = null,
    private val onJokesClick: ((Int, Jokes) -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val VIEW_TYPE_CATEGORY = 1
        const val VIEW_TYPE_JOKES = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_CATEGORY -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_category, parent, false)
                CategoryViewHolder(view)
            }
            VIEW_TYPE_JOKES -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_jokes, parent, false)
                JokeViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_category, parent, false)
                CategoryViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (itemListCategory.isNotEmpty()) itemListCategory.size else itemListJokes.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is CategoryViewHolder -> {
                holder.bindItem(position, itemListCategory[position], onCategoryClick)
            }
            is JokeViewHolder -> {
                holder.bindItem(position, itemListJokes[position], onJokesClick)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (itemListCategory.isNotEmpty()) VIEW_TYPE_CATEGORY else VIEW_TYPE_JOKES
    }

    inner class CategoryViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(
            pos: Int,
            item: String,
            onCategoryClick: ((Int, String) -> Unit)? = null
        ) {
            itemView.tvCategory.text = item
            containerView.setOnClickListener {
                onCategoryClick?.invoke(pos, item)
            }
        }
    }

    inner class JokeViewHolder(override val containerView: View) :
        RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bindItem(
            pos: Int,
            item: Jokes,
            onJokesClick: ((Int, Jokes) -> Unit)?
        ) {
            itemView.tvJoke.text = item.value
            Glide.with(context)
                .load(item.icon_url)
                .error(R.drawable.ic_empty_picture)
                .into(itemView.ivJoke)
            containerView.setOnClickListener {
                onJokesClick?.invoke(pos, item)
            }
        }
    }

}