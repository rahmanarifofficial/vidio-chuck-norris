package com.rahmanarifofficial.vidiochucknorris.model

data class ResJokes(
    val result: List<Jokes>,
    val total: Int
)