package com.rahmanarifofficial.vidiochucknorris

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.app.NavUtils
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.rahmanarifofficial.vidiochucknorris.datasource.Status
import com.rahmanarifofficial.vidiochucknorris.utils.BaseActivity
import com.rahmanarifofficial.vidiochucknorris.utils.ViewModelFactory
import com.rahmanarifofficial.vidiochucknorris.viewmodel.JokesViewModel
import kotlinx.android.synthetic.main.activity_detail_joke.*
import kotlinx.android.synthetic.main.activity_detail_joke.layoutError
import kotlinx.android.synthetic.main.activity_detail_joke.progressBar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.item_list_jokes.*


class DetailJokeActivity : BaseActivity() {
    private lateinit var jokesViewModel: JokesViewModel
    private var category = ""
    override fun provideLayout() {
        setContentView(R.layout.activity_detail_joke)
    }

    override fun initObject(bundle: Bundle?) {
        val factory = ViewModelFactory.getInstance()
        jokesViewModel = ViewModelProvider(this, factory)[JokesViewModel::class.java]
    }

    override fun initUI(bundle: Bundle?) {
        supportActionBar?.title = "Random Joke"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun initData(bundle: Bundle?) {
        if (intent != null) {
            category = intent.getStringExtra("CATEGORY_BUNDLE") ?: ""
        }
        loadData(category)
    }

    override fun initListener(bundle: Bundle?) {
        buttonRefreshError?.setOnClickListener {
            loadData(category)
        }
    }

    private fun loadData(category: String) {
        jokesViewModel.getJokesByCategory(category).observe(this, {
            it?.let { res ->
                when (res.status) {
                    Status.SUCCESS -> {
                        progressBar?.visibility = View.GONE
                        layoutError?.visibility = View.GONE
                        itemJoke?.visibility = View.VISIBLE
                        val item = res.data
                        item?.let {
                            tvJoke.text = item.value
                            Glide.with(this)
                                .load(item.icon_url)
                                .error(R.drawable.ic_empty_picture)
                                .into(ivJoke)
                        } ?: apply {
                            layoutError?.visibility = View.VISIBLE
                        }
                    }
                    Status.ERROR -> {
                        progressBar?.visibility = View.GONE
                        itemJoke?.visibility = View.GONE
                        layoutError?.visibility = View.VISIBLE
                    }
                    Status.LOADING -> {
                        progressBar?.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}