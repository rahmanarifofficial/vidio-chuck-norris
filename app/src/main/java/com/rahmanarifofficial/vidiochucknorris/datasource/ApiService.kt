package com.rahmanarifofficial.vidiochucknorris.datasource

import androidx.lifecycle.LiveData
import com.rahmanarifofficial.vidiochucknorris.model.Jokes
import com.rahmanarifofficial.vidiochucknorris.model.ResJokes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ApiService {

    @GET("jokes/search")
    fun searchJokes(@Query("query") query: String): LiveData<ApiResponse<ResJokes>>

    @GET("jokes/random")
    fun getJokesByCategory(@Query("categories") category: String): LiveData<ApiResponse<Jokes>>

    @GET("jokes/categories")
    fun categoryJokes(): LiveData<ApiResponse<List<String>>>
}