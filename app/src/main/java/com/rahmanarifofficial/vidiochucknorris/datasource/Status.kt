package com.rahmanarifofficial.vidiochucknorris.datasource

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}