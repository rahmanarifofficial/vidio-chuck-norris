package com.rahmanarifofficial.vidiochucknorris.datasource

import com.rahmanarifofficial.vidiochucknorris.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class DataSource {
    companion object {
        private fun service(): Retrofit {
            val service: Retrofit =
                Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
                    .build()
            return service
        }
    }

    open class Service {
        companion object {
            val api: ApiService by lazy {
                service().create(ApiService::class.java)
            }
        }
    }
}