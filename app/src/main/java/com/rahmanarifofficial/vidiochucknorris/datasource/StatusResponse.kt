package com.rahmanarifofficial.vidiochucknorris.datasource

enum class StatusResponse {
    SUCCESS,
    EMPTY,
    ERROR
}